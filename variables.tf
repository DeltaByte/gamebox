variable "zone_id" {
  type        = string
  description = "Cloudflare Zone ID"
}

variable "discord_url" {
  type        = string
  description = "Discord invite URL"
  default     = "https://discord.gg/CNx4ty2W"
}

/**
 * Server
 */
variable "server_ip" {
  type        = string
  description = "Public IP address of server"
}

variable "server_ssh_port" {
  type        = string
  description = "SSH port for server"
  default     = 22
}

variable "server_ssh_user" {
  type        = string
  description = "SSH user for server"
  default     = "root"
}

/**
 * Minecraft
 */
variable "mc_modded_ops" {
  type        = list(string)
  description = "FTB server operators"
  default     = [
    "DeltaByte_",
    "OxidizedPickle"
  ]
}

variable "mc_bedrock_ops" {
  type        = list(string)
  description = "Bedrock server operators (XUID)"
  default     = [
    "2533274849230625" // TheDeltaByte
  ]
}

variable "mc_discord_token" {
  type        = string
  description = "Discord API token for bot"
  default     = ""
}

variable "mc_discord_admin_role" {
  type        = string
  description = "Discord role ID for server admins"
  default     = "528762673085808640"
}

variable "mc_modded_discord_channel" {
  type        = string
  description = "Discord channel ID for bot"
  default     = "523267223549378570"
}
