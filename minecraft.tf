locals {
  mc_modded_memory  = 12288 // 12G
  mc_bedrock_memory = 4096 // 4G
  mc_discord_mod    = "dcintegration-forge-2.1.1-1.16.jar"
}

resource "cloudflare_record" "minecraft" {
  zone_id = var.zone_id
  name    = "mc"
  type    = "A"
  value   = var.server_ip
  ttl     = 3600
}

/**
 * Modded
 */
resource "docker_volume" "mc_modded" {
  name = "mc-modded"
}

resource "docker_container" "mc_modded" {
  image   = "itzg/minecraft-server:java8-multiarch"
  name    = "mc-modded"
  restart = "unless-stopped"
  memory  = local.mc_modded_memory

  ports {
    internal = 25565
    external = 25565
    protocol = "tcp"
    ip       = var.server_ip
  }

  env = [
    "EULA=TRUE",
    "TYPE=FTBA",

    // java opts
    "MEMORY=${local.mc_modded_memory}M",
    "INIT_MEMORY=${ceil(local.mc_modded_memory / 2)}M",
    "MAX_MEMORY=${local.mc_modded_memory}M",
    "USE_AIKAR_FLAGS=TRUE",

    // general config
    "TZ=Europe/London",
    "ENABLE_ROLLING_LOGS=TRUE",

    // direwolf20 @ 1.9.0
    "FTB_MODPACK_ID=79",
    "FTB_MODPACK_VERSION_ID=2045",

    // re-generate config on startup
    "OVERRIDE_SERVER_PROPERTIES=TRUE",
    "OVERRIDE_OPS=TRUE",

    // server.properties
    "ALLOW_FLIGHT=TRUE",
    "DIFFICULTY=hard",
    "SERVER_NAME=DeltaCraftFTB",
    "OPS=${join(",", var.mc_modded_ops)}",
    "MAX_WORLD_SIZE=15000",
    "SPAWN_PROTECTION=16",
    "VIEW_DISTANCE=10",
    "MOTD=DeltaByte's FTB server | ${var.discord_url}",
    "LEVEL_TYPE=LARGEBIOMES",
  ]

  volumes {
    volume_name    = "mc-modded"
    container_path = "/data"
  }

  upload {
    source      = "./downloads/${local.mc_discord_mod}"
    source_hash = filesha256("./downloads/${local.mc_discord_mod}")
    file        = "/mods/${local.mc_discord_mod}"
  }

  upload {
    file           = "/config/Discord-Integration.toml"
    content_base64 = base64encode(templatefile("./config/mc-modded/Discord-Integration.toml.tpl", {
      discord_url     = var.discord_url
      bot_token       = var.mc_discord_token
      bot_channel     = var.mc_modded_discord_channel
      admin_role      = var.mc_discord_admin_role
      # custom_commands = data.external.mc_modded_discord_commands.result.value
      custom_commands = file("./tmp-commands.json")
    }))
  }
}

// loads json file, escapes it into one stirng, then wraps it in { value: "" } due to terraform interface
# data "external" "mc_modded_discord_commands" {
#   program = ["jq", ". | @json | { value: . }", "./config/mc-modded/Discord-Integration-Commands.json"]
# }

/**
 * Bedrock
 */
resource "docker_volume" "mc_bedrock" {
  name = "mc-bedrock"
}

resource "docker_container" "mc_bedrock" {
  image = "itzg/minecraft-bedrock-server"
  name  = "mc-bedrock"
  restart = "unless-stopped"
  memory  = local.mc_bedrock_memory


  ports {
    internal = 19132
    external = 19132
    protocol = "udp"
    ip       = var.server_ip
  }

  env = [
    "EULA=TRUE",
    "VERSION=LATEST",

    // general config
    "TZ=Europe/London",

    // server.properties
    "DIFFICULTY=hard",
    "SERVER_NAME=DeltaCraft",
    "OPS=${join(",", var.mc_bedrock_ops)}",
    "VIEW_DISTANCE=10",
    "MOTD=DeltaByte's Bedrock server | ${var.discord_url}"
  ]

  volumes {
    volume_name    = "mc-bedrock"
    container_path = "/data"
  }
}