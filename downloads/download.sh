#! /bin/sh

download() {
  local URL=$1
  local FILE="${URL##*/}"

  if test -f "$FILE"; then
    echo "File already exists, skipping. [$FILE]"
  else
    echo "File does not exist, downloading. [$FILE]"
    curl -sS $URL -o $FILE
  fi
}

# minecraft
download https://media.forgecdn.net/files/3258/592/dcintegration-forge-2.1.1-1.16.jar