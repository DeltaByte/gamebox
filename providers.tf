terraform {
  backend "http" {}

  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "~> 2.11"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 2.20"
    }
  }
}

// assumes that the SSH key has already be configured on the runner
provider "docker" {
  host = "ssh://${var.server_ssh_user}@${var.server_ip}:${var.server_ssh_port}"
}

// auth set via environment
provider "cloudflare" {}
