# Gamebox

Deployment and configuration of various game servers, this is accomplished by running all of the servers on a docker host, with all of the containers being deployed via Terraform.

## Running terraform locally

This project uses the [gitlab state](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html), below is an example `_override.tf` that will allow the state to be managed locally.

```hcl
terraform {
  backend "http" {
    address  = "https://gitlab.com/api/v4/projects/26312316/terraform/state/default"
    username = "<YOUR-USERNAME>"
    password = "<YOUR-ACCESS-TOKEN>"

    lock_address = "https://gitlab.com/api/v4/projects/26312316/terraform/state/default/lock"
    lock_method  = "POST"

    unlock_address = "https://gitlab.com/api/v4/projects/26312316/terraform/state/default/lock"
    unlock_method  = "delete"

    retry_wait_min = 5
  }
}
```
